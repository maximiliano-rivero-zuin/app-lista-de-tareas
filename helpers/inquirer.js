const inquirer = require('inquirer');
const colors = require('colors');


const menu = () =>{
    
    console.clear()

    console.log(`${colors.cyan('=========================')}`)
    console.log(`${colors.magenta('  Seleccione una opción')}`)
    console.log(`${colors.cyan('=========================')}`)

    const questions = [
        {
            type: 'list',
            name: 'options',
            message: `¿Qué desea hacer? `,
            choices: [
                {
                    value: '1',
                    name: `${'1.'.magenta} Crear tarea`
                },
                {
                    value: '2',
                    name: `${'2.'.magenta} Listar tareas`
                },
                {
                    value: '3',
                    name: `${'3.'.magenta} Listar tareas completadas`
                },
                {
                    value: '4',
                    name: `${'4.'.magenta} Listar tareas pendientes`
                },
                {
                    value: '5',
                    name: `${'5.'.magenta} Completar tarea(s)`
                },
                {
                    value: '6',
                    name: `${'6.'.magenta} Borrar tarea`
                },
                {
                    value: '0',
                    name: `${'0.'.magenta} Salir`
                },
            ],
            filter(value) {
              return value;
            },
          }
    ]

    return new Promise((resolve, reject) => {
    inquirer
        .prompt(questions)
        .then((answers) => {
            resolve(answers.options)
        })
        .catch((error) => {
            if (error.isTtyError) {
                reject(error)
            } else {
                reject(error)
            }
        });
    })
}

const pausa = async() => {
    const question = [
        {
            type: 'input',
            name: 'enter',
            message: `Presione ${colors.cyan('ENTER')} para continuar `,
            validate() {
                return true;
            }
        }
    ];
    await inquirer.prompt(question);
}

const listadoTareasBorrar = async( tareas ) => {

    const tasks = tareas.map( (tarea, index) => {

        const ind = colors.green(index+1)
        const {id, desc} = tarea

        return {
            value: id,
            name: `${ind} ${desc}`
        }
    })

    tasks.unshift({
        value: '0',
        name: '0 '.green + 'Cancelar'
    })

    const questions = [
        {
            type: 'list',
            name: 'id',
            message: 'Borrar tarea ',
            choices: tasks
        }
    ]

    const { id } = await inquirer.prompt(questions)

    return id;
}

const confirmar = async(mensaje) => {

    const question = [
        {
            type: 'confirm',
            name: 'ok',
            message: mensaje
        }
    ]

    const { ok } = await inquirer.prompt(question)
    return ok;

}

const leerInput = async() => {
    const question = [
        {
            type: 'input',
            name: 'desc',
            message: 'Descripción: ',
            validate( value ){
                if (value.length === 0) {
                    return 'Por favor ingrese un valor';
                }
                return true;
            }
        }
    ];
    const { desc } = await inquirer.prompt(question);
    return desc;
}

const mostrarListadoChecklist = async( tareas ) => {

    const tasks = tareas.map( (tarea, index) => {

        const ind = colors.green(index+1)
        const {id, desc, completadoEn} = tarea

        return {
            value: id,
            name: `${ind} ${desc}`,
            checked: completadoEn ? true : false
        }
    })

    const question = [
        {
            type: 'checkbox',
            name: 'ids',
            message: 'Selecciones',
            choices: tasks
        }
    ]

    const { ids } = await inquirer.prompt(question)

    return ids;
}   

module.exports = {
    menu,
    pausa,
    leerInput,
    listadoTareasBorrar,
    confirmar,
    mostrarListadoChecklist
}