const fs = require('fs') //module file system

const file = './database/tareas.json'

const guardarDB = (data) => {

    const dataStr = JSON.stringify(data)

    fs.writeFile(file, dataStr, (err) => {
        if (err) throw err;
      }) 

}

const leerDB = () => {

    if (!fs.existsSync(file)) {
        return null;
    }

    const stringDB = fs.readFileSync(file, { encoding: 'utf-8' })
    const jsonDB = JSON.parse(stringDB)
    
    return jsonDB;
}

module.exports = {
    guardarDB: guardarDB,
    leerDB: leerDB
}