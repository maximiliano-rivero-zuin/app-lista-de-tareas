const tarea = require('./tarea');
const colors = require('colors');
const { Tarea } = require('./tarea');

class Tareas {

    constructor(){
        this._listado = {}
    }

    toggleCompletadas( ids ){

        ids.forEach( id => {
            
            const tarea = this._listado[id]
            if( !tarea.completadoEn ){
                tarea.completadoEn = new Date().toISOString()
            }
        })

        this.listadoArr.forEach( tarea => {
            const { id } = tarea
            if ( !ids.includes(id) ) {
                this._listado[id].completadoEn = null
            }
        })
    }

    borrarTarea(id){
        if ( this._listado[id]) {
            delete this._listado[id]
            console.log(`Tarea borrada`)
        }
    }

    cargarTareasFromArray( tareas ){
        tareas.forEach( tarea => {
            this._listado[tarea.id] = tarea 
        })
    }

    listadoCompleto(){
        this.listadoArr.forEach( (tarea, index) => {
            const { desc, completadoEn } = tarea
            const state = completadoEn ? 'Completada'.green : 'Pendiente'.red

            console.log(`${colors.green(index+1)}. ${desc} :: ${state}`) 
        })
    }

    listarPendientesCompletadas( bool = true ){ 
        // true -> lista completadas
        // false -> lista pendientes
        let index = 0
        this.listadoArr.forEach( (tarea) => {
            const { desc, completadoEn } = tarea
            const state = completadoEn ? 'Completada'.green : 'Pendiente'.red
            if (bool && completadoEn) {
                index += 1
                console.log(`${colors.green(index)}. ${(desc).cyan} :: ${state}`)
            } else if (!bool && !completadoEn) {
                index += 1
                console.log(`${colors.green(index)}. ${(desc).cyan} :: ${state}`)
            }
        })
    }

    crearTarea(desc) {
        let newTarea = new Tarea(desc);
        this._listado[newTarea.id] = newTarea;
        console.log(newTarea.desc)
    }

    get listadoArr() {
        //llenar un array con las propiedades
        const _listadoArr = []
        Object.keys(this._listado).forEach(key => {
            const tareaObj = this._listado[key]
            _listadoArr.push(tareaObj)
        })

        return _listadoArr 
    }

    

}

module.exports = {
    Tareas: Tareas
}