const { v4: uuidv4 } = require('uuid');

class Tarea {

    id = ''; //identificador
    desc = ''; //descripcion de la tarea
    completadoEn = null; //fecha en que se completó - por defecto null

    constructor(desc){
        this.id = uuidv4();  // example '1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed'
        this.desc = desc;
    }

}


module.exports = {
    Tarea: Tarea
}