# App Lista de Tareas

Crear una **app de consola** utilizando [Inquirer.js](https://www.npmjs.com/package/inquirer) que pueda **administrar** una **lista de tareas** con las opciones:

- 1. Crear tarea
- 2. Listar tareas
- 3. Listar tareas completadas
- 4. Listar tareas pendientes
- 5. Completar tarea(s)
- 6. Borrar tarea
- 0. Salir
