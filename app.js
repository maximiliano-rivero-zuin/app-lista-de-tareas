const { guardarDB, leerDB } = require('./helpers/guardarArchivo');
const { menu, leerInput, pausa, listadoTareasBorrar, confirmar, mostrarListadoChecklist } = require('./helpers/inquirer');
const { Tareas } = require('./models/tareas');

console.clear()

const main = async () => {
    let opt = ''
    const tareas = new Tareas();
    
    const databaseTareas = leerDB();
    if (databaseTareas) {
        tareas.cargarTareasFromArray(databaseTareas)
    }
    

    do {
        
        /*inquirer*/
        opt = await menu()
            .catch((err) => {console.log(err)});
        
            switch (opt) {
                case '1': // Crear tarea
                    let desc = await leerInput()
                    await tareas.crearTarea(desc);
                    guardarDB(tareas.listadoArr)
                    break;
                case '2': // Listar todas las tareas
                    tareas.listadoCompleto()
                    break;
                case '3': // Listar tareas completadas
                    tareas.listarPendientesCompletadas(true)
                    break;
                case '4': // Listar tareas pendientes
                    tareas.listarPendientesCompletadas(false)
                    break;
                case '5': // Completar tareas
                    const ids = await mostrarListadoChecklist(tareas.listadoArr)
                    tareas.toggleCompletadas(ids)
                    guardarDB(tareas.listadoArr)
                    break;
                case '6': // Borrar tarea
                    const id = await listadoTareasBorrar(tareas.listadoArr)
                        const ok = await confirmar('¿Está seguro?')
                        if (ok) {
                            tareas.borrarTarea(id)
                            guardarDB(tareas.listadoArr)
                        }  
                    break;
                case '0': // Salir
                    break;
              }

        await pausa()

    } while (opt !== '0')
}

main()


